#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int capacite;
    int taille;
    // your code
};


void initialise(Liste* liste)
{
   liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier == nullptr)
        return true;
    return false;
}

void ajoute(Liste* liste, int valeur)
{
    Noeud *noeud = new Noeud; //on créée un nouveau noeud
    noeud->donnee=valeur;
    noeud->suivant=liste->premier; //le pointeur vers le noeud suivant est égal au premier de la liste
    liste->premier=noeud;//le noeud se met au début de la liste
}

void affiche(const Liste* liste)
{
    if(liste->premier !=nullptr){
        Noeud *noeud = liste->premier;
        while (noeud!=nullptr){
            cout << noeud->donnee<<endl;
            noeud = noeud->suivant;
        }
    }

}

int recupere(const Liste* liste, int n)//retourne le nième entier de la liste
{
    Noeud* iterateur = liste->premier;
    if (n==0)
    {
        return iterateur->donnee;
    }
    for (int i =0 ; i<n+1 ; i++){

        iterateur = iterateur->suivant;
    }
    return iterateur->donnee;
}

int cherche(const Liste* liste, int valeur)
{
//    int trouve = 0;
    Noeud *iterateur = liste->premier;
    for(int i =0; iterateur!=nullptr; i++){
        if (iterateur->donnee == valeur){
            cout << "valeur trouvée à l'adresse" << &iterateur << endl;
//            trouve = 1;
            return i;
        }
        iterateur=iterateur->suivant;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur) //redefini la nieme valeur de la structure avec valeur
{
    Noeud* iterateur = liste->premier;
    if (n==0)
    {
        iterateur->donnee=valeur;
    }
    for (int i =0 ; i<n ; i++){

        iterateur = iterateur->suivant;
    }
    iterateur->donnee = valeur;

}

void initialise(DynaTableau* tableau, int capacite)
{
    tableau->donnees=new int[capacite];
    tableau->taille = 0;
    tableau->capacite=capacite;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if(tableau->taille+1>tableau->capacite){ // si la taille du tableau est supérieure à la capacité
        tableau->capacite+=5;//on augmente la capacité de 5
        int* nouveauTableau = new int[tableau->capacite];//on créé un nouveau tableau avec cette nouvelle capacité
        for(int i =0; i<tableau->taille ; i++){ //on copîe l'ancien tableau dans le nouveau
            nouveauTableau[i] = tableau->donnees[i];
        }
        nouveauTableau[tableau->taille]=valeur;//on ajoute la nouvelle valeur
        tableau->taille++;
        tableau->donnees=nouveauTableau;//on met les valeurs du nouveau tableau
    }
    else{
        tableau->donnees[tableau->taille]=valeur;
        tableau->taille++;
    }
}


bool est_vide(const DynaTableau* liste)
{
    if(liste->taille==0){
        return true;
    } else {
        return false;
    }

}

void affiche(const DynaTableau* tableau)
{
    for(int i = 0; i<=tableau->taille; i++){
        cout << tableau->donnees[i] << endl;
    }

}

int recupere(const DynaTableau* tableau, int n) //retourne le nième entier du tableau
{
    if (n<=tableau->capacite)
    {
        return tableau->donnees[n];
    }
    else
    {
        return 0;
    }
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i =0; i<tableau->taille ; i++){
        if(tableau->donnees[i]==valeur){
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    tableau->donnees[n]=valeur;

}

//void pousse_file(DynaTableau* liste, int valeur)
// file : les éléments sont ajoutés à la fin et retirés au début -> FIFO -> le premier élément ajouté est le premier à être retiré
void pousse_file(Liste* liste, int valeur)
{
    Noeud *noeud =new Noeud;
    noeud->donnee=valeur;
    noeud->suivant=nullptr;
    if (liste->premier == nullptr) {
            // Si la liste est vide, le nouveau nœud devient le premier
            liste->premier = noeud;
    } else {
             // Sinon, ajoute le nouveau nœud à la fin de la liste
        Noeud *derniernoeud = liste->premier;
        while (derniernoeud->suivant !=nullptr){
            derniernoeud=derniernoeud->suivant;
        }
        derniernoeud->suivant=noeud;
    }

}

//int retire_file(Liste* liste)

int retire_file(Liste* liste)
{
    //int valeur = liste->premier->donnee;
   // liste->premier=liste->premier->suivant;
    //return valeur;
    int temp=0;
    if (liste->premier!=NULL){
        Noeud *valeur = liste->premier;
        temp = valeur->donnee;
        liste->premier=valeur->suivant;
        free(valeur);
    }
    return temp;
}

//void pousse_pile(DynaTableau* liste, int valeur)
// pile : Les éléments sont ajoutés à la pile en haut de la pile et sont retirés également depuis le haut de la pile.
//mieux pour la liste chaînée
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *newNode = new Noeud;
    newNode->donnee = valeur;
    newNode->suivant = liste->premier;
    liste->premier = newNode;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    if (liste->premier != nullptr) {
        int valeur = liste->premier->donnee;
        liste->premier = liste->premier->suivant;
        return valeur;
    }
    return 0;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);

    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    for (int i=1; i<=7; i++) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }

    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
     std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
   std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
