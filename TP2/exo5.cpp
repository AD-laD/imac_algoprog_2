#include <QApplication>
#include <time.h>
#include "tp2.h"

MainWindow* w=nullptr;

void merge(Array& first, Array& second, Array& result);

void splitAndMerge(Array& origin)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(origin.size() <= 1){
        return;
    }
	// initialisation
    Array& first = w->newArray(origin.size()/2);
    Array& second = w->newArray(origin.size()-first.size());
    int mid = origin.size() / 2;
    for (int i = 0; i < mid; i++) {
        first[i] = origin[i];
    }
    for (int i = 0; i < second.size(); i++) {
        second[i] = origin[mid + i];
    }

    splitAndMerge(first);
    splitAndMerge(second);
    merge(first, second, origin);
}

void merge(Array& first, Array& second, Array& result)
{
    int index=0;
    int index1=0;
    int index2=0;

    while(index1<first.size() && index2<second.size()){
        if(first[index1]<second[index2]){
                result[index++]=first[index1];
                index1++;
        }
        else{
                result[index++]=second[index2];
                index2++;
        }
    }
    if(index1<first.size()){
        for(int i=index1;i<first.size();i++){
                result[index++]=first[index1];

        }
    }
    if(index2<second.size()){
        for(int i=index2;i<second.size();i++){
            result[index++]=second[index2];
        }
    }
}

void mergeSort(Array& toSort)
{
    splitAndMerge(toSort);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(mergeSort);
	w->show();

	return a.exec();
}
