#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
    for (uint i=1; i<toSort.size(); i++)
    {
        for (uint j=0; j<toSort.size()-1; j++)
        {
            if (toSort[j+1] < toSort[j]){ // si l'entier indice j+1 est inférieur à l'entier indice j
                toSort.swap(j+1, j);//on les échange
            }
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
