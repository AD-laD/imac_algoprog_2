#include <QApplication>
#include <time.h>

#include "tp2.h"


MainWindow* w=nullptr;

void recursivQuickSort(Array& toSort, int size)
{
	// stop statement = condition + return (return stop the function even if it does not return anything)
    if(size <= 1){
        return;
    }
    int pivot = toSort[0]; //on definit notre pivot
	Array& lowerArray = w->newArray(size);
	Array& greaterArray= w->newArray(size);
	int lowerSize = 0, greaterSize = 0; // effectives sizes

	// split

    for(int i=1; i<size; i++){
        if(pivot>toSort[i]) { //si la valeur de toSort est inférieure au pivot
            lowerArray[lowerSize]=toSort[i]; //le tableau lowerarray se remplit
            lowerSize++;
        }
        else {
            greaterArray[greaterSize]=toSort[i]; //le tableau lowerarray se remplit
            greaterSize++;
        }
    }
	
	// recursiv sort of lowerArray and greaterArray
    recursivQuickSort(lowerArray, lowerSize);
    recursivQuickSort (greaterArray, greaterSize);

	// merge
    for(int i =0; i<lowerSize ; i++){
        toSort.set(i, lowerArray[i]); //on remplace la valeur dans le tableau toSort (début du tableau
    }
    toSort.set(lowerSize, pivot); //on met le pivot après les plus petites valeurs
    for (int i =0 ; i <greaterSize ; i++) {
        toSort.set(i+lowerSize+1, greaterArray[i]);
    }

}

void quickSort(Array& toSort){
	recursivQuickSort(toSort, toSort.size());
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 50;
    w = new TestMainWindow(quickSort);
	w->show();

	return a.exec();
}
