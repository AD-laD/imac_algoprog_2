#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort){
	// selectionSort
    // on compare le premier éléemnt du tableau avec le reste du tableau pour savoir si un élément est plus petit, puis on compare le deuxième élément avec la suite du tableau, et ainsi de suite
    for (int i=0; i<toSort.size(); i++)
       {

           int mini=toSort[i];

           for (int j=i+1; j<toSort.size(); j++)
           {
               if (toSort[j] < mini)
               {
                   mini = toSort[j];
                   toSort[j]=toSort[i];
                   toSort[i]=mini;
               }
           }
       }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
