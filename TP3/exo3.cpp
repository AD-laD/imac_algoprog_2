#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value=value;
        this->left = 0;
        this->right= 0;
        //this == le premier noeud
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        SearchTreeNode *noeud = new SearchTreeNode(value); //on créée un nouveau noeud
        if(this->value<=value) {
            if(this->left ==nullptr){ //s'il n'y a pas encore de noeud à gauche
                this->left=noeud;
            }
            else {
                this->left->insertNumber(value);
            }

        }
        if(this->value >=value){
            if(this->right==nullptr){
                this->right=noeud;
            }
            else {
                this->right->insertNumber(value);
            }

        }
    }

	uint height() const	{
        // should return the maximum height between left child and
        // right child +1 for itself. If there is no child, return
        // just 1

        int heightLeft, heightRight;

        if (this->left==nullptr && this->right==nullptr)
        {
            return 1;
        }

        else
        {
            if (this->left==nullptr)
            {
                heightLeft = 0;
            }
            else
            {
                heightLeft = this->left->height();
            }

            if (this->right==nullptr)
            {
                heightRight = 0;
            }
            else
            {
                heightRight = this->right->height();
            }

            if (heightLeft>=heightRight)
            {
                return 1+heightLeft;
            }
            else
            {
                return 1+heightRight;
            }
        }

    }

	uint nodesCount() const {
        // should return the sum of nodes within left child and
        // right child +1 for itself. If there is no child, return
        // just 1
        int nodeLeft, nodeRight;

       if (this->left==nullptr && this->right==nullptr)
       {
           return 1;
       }

       else
       {
           if (this->left==nullptr)
           {
               nodeLeft = 0;
           }
           else
           {
               nodeLeft = this->left->nodesCount();
           }

           if (this->right==nullptr)
           {
               nodeRight = 0;
           }
           else
           {
               nodeRight = this->right->nodesCount();
           }

           return nodeRight+nodeLeft+1;
       }
	}

	bool isLeaf() const {
        // return True if the node is a leaf (it has no children)
        if((this->left==nullptr) && (this->right==nullptr)){
            return true;
        } else {
            return false;
        }

	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if (this->isLeaf()) {
           leaves[leavesCount] = this;
           leavesCount++;
        } else {
            if (this->right) {
               this->right->allLeaves(leaves, leavesCount);
            }

            if (this->left) {
               this->left->allLeaves(leaves, leavesCount);
            }
        }

	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if (this->left) {
            this->left->inorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount++;

        if (this->right) {
           this->right->inorderTravel(nodes, nodesCount);
        }

	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel
        nodes[nodesCount] = this;
        nodesCount++;

        if (this->left) {
            this->left->preorderTravel(nodes, nodesCount);
        }

        if (this->right) {
            this->right->preorderTravel(nodes, nodesCount);
        }

	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if (this->left) {
            this->left->postorderTravel(nodes, nodesCount);
        }

        if (this->right) {
            this->right->postorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount++;

	}

	Node* find(int value) {
        // find the node containing value
        Node *node = nullptr;

        if (value == this->value) {
            node = this;
        } else if (value < this->value) {
            node = this->left->find(value);
        } else if (value > this->value) {
            node = this->right->find(value);
        }
        return node;

	}

    void reset()
    {
        if (left != NULL)
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
