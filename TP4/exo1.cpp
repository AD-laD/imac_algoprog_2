#include "tp4.h"
#include "mainwindow.h"

#include <QApplication>
#include <time.h>
#include <stdio.h>

MainWindow* w = nullptr;
using std::size_t;
using std::string;

int Heap::leftChildIndex(int nodeIndex)
{
    return nodeIndex*2+1;
}

int Heap::rightChildIndex(int nodeIndex)
{
    return nodeIndex*2+2;
}

void Heap::insertHeapNode(int heapSize, int value)
{

	int i = heapSize;
    this->get(i)=value; // use (*this)[i] or this->get(i) to get a value at index i
    while(i >0 && this->get(i)>this->get((i-1)/2)){
        int temp = this->get(i);
        this->get(i) = this->get((i-1)/2);
        this->get((i-1)/2) = temp;
//        swap(this->get(i), this->get((i-1)/2));
        i=(i-1)/2;
    }

}

void Heap::heapify(int heapSize, int nodeIndex)
{
    //si le noeud à l'indice nodeIndex n'est pas supérieur à ses enfants, reconstruit le tas à partir de cet index
	// use (*this)[i] or this->get(i) to get a value at index i

	int i_max = nodeIndex;

//    if(i_max !=i){
//        swap(this->get(i), this->get(i_max));

//        heapify(heapSize, i_max);
//    }

    if (leftChildIndex(nodeIndex) < heapSize && this->get(i_max)< this->get(leftChildIndex(nodeIndex))) //si index de gauche est inférieur à la taille
       {
           i_max=leftChildIndex(nodeIndex);
       }

       if (rightChildIndex(nodeIndex) < heapSize && this->get(i_max) < this->get(rightChildIndex(nodeIndex)))
       {
           i_max=rightChildIndex(nodeIndex);
       }

       if (i_max!=nodeIndex)
       {
           swap(nodeIndex,i_max);
           heapify(heapSize,i_max);
       }

}

void Heap::buildHeap(Array& numbers)
{
    //construit un tas à partir des valeurs de numbers
    for (int i=0;i<numbers.size();i++)
    {
        insertHeapNode(i, numbers[i]);
    }

}

void Heap::heapSort()
{
    //construit un tableau trié à partir d'un tas heap
    for(int i=this->size()-1; i>0; i--){
        swap(0,i);
        heapify(i,0);
    }

}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    MainWindow::instruction_duration = 50;
    w = new HeapWindow();
	w->show();

	return a.exec();
}
